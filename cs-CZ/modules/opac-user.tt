[% USE Koha %]
[% USE KohaDates %]

[% INCLUDE 'doc-head-open.inc' %]
<title>[% IF ( LibraryNameTitle ) %][% LibraryNameTitle %][% ELSE %]Koha on-line[% END %] katalog &rsaquo; Vaše knihovna</title>
[% INCLUDE 'doc-head-close.inc' %]
[% BLOCK cssinclude %][% END %]
</head>
[% INCLUDE 'bodytag.inc' bodyid='opac-user' bodyclass='scrollto' %]
[% INCLUDE 'masthead.inc' %]

<div class="main">
 <ul class="breadcrumb">
 <li><a href="/cgi-bin/koha/opac-main.pl">Koha</a> <span class="divider">&rsaquo;</span></li>
 <li>[% FOREACH BORROWER_INF IN BORROWER_INFO %]<a href="/cgi-bin/koha/opac-user.pl">[% INCLUDE 'patron-title.inc' category_type = BORROWER_INF.category_type firstname = BORROWER_INF.firstname surname = BORROWER_INF.surname othernames = BORROWER_INF.othernames cardnumber = BORROWER_INF.cardnumber %]</a>[% END %] <span class="divider">&rsaquo;</span></li>
 <li><a href="#">Přehled účtu</a></li>
 </ul>

 <div class="container-fluid">
 <div class="row-fluid">
 <div class="span2">
 <div id="navigation">
 [% INCLUDE 'navigation.inc' IsPatronPage=1 %]
 </div>
 </div>
 <div class="span10">
 <div id="userdetails" class="maincontent">
 [% IF ( bor_messages ) %]
 <div class="alert alert-info">
 <h3>Zprávy pro Vás</h3>
 <ul>
 [% FOREACH bor_messages_loo IN bor_messages_loop %]
 <li>
 <strong>[% bor_messages_loo.message %]</strong><br>
 &nbsp;&nbsp;&nbsp;<i>Dne [% bor_messages_loo.message_date | $KohaDates %] zapsala knihovna [% bor_messages_loo.branchname %]</i>
 </li>
 [% END %]

 [% IF ( opacnote ) %]<li>[% opacnote %]</li>[% END %]
 </ul>
 </div>
 [% END # / IF bor_messages %]
 [% FOREACH BORROWER_INF IN BORROWER_INFO %]
 <h2>Přihlášen jako [% INCLUDE 'patron-title.inc' category_type = BORROWER_INF.category_type firstname = BORROWER_INF.firstname surname = BORROWER_INF.surname othernames = BORROWER_INF.othernames cardnumber = BORROWER_INF.cardnumber %] </h2>

 <p><a href="/cgi-bin/koha/opac-main.pl?logout.x=1">Klikněte sem pokud nejste [% BORROWER_INF.title %] [% INCLUDE 'patron-title.inc' category_type = BORROWER_INF.category_type firstname = BORROWER_INF.firstname surname = BORROWER_INF.surname othernames = BORROWER_INF.othernames cardnumber = BORROWER_INF.cardnumber %]</a></p>

 [% IF ( patronupdate ) %]<div class="alert alert-info"><h3>Děkujeme!</h3><p>Vaše opravy byly odeslány a Váš záznam bude pracovníky knihovny co nejdříve aktualizován.</p></div>[% END %]

 [% IF ( BORROWER_INF.warndeparture ) %]
 <div class="alert" id="warndeparture">
 <strong>Upozornění:</strong><span> Vaše registrace vyprší dne <span id="warndeparture_date">[% BORROWER_INF.warndeparture | $KohaDates %]</span>. Pro více informací prosím kontaktujte knihovnu.</span>
 [% IF ( BORROWER_INF.returnbeforeexpiry ) %]<span id="warndeparture_returnbeforeexpiry"> Poznamenejte si, že všechny vypůjčené položky musí být vráceny dříve, než vyprší platnost vašeho průkazu.</span>[% END %]
 </div>
 [% END %]

 [% IF ( BORROWER_INF.warnexpired ) %]
 <div class="alert" id="warnexpired">
 <strong>Upozornění: </strong><span>Vaše registrace vypršela k datu [% BORROWER_INF.warnexpired | $KohaDates %]. Pro pro obnovení účtu kontaktuje knihovnu.</span>
 </div>
 [% END %]

 [% IF ( RENEW_ERROR ) %]
 <div class="dialog alert">
 <strong>Upozornění:</strong>
 <span>
 Prodloužení vaší výpůjčky selhalo z následujících důvodů: [% FOREACH error IN RENEW_ERROR.split('\|') %] [% IF error == 'card_expired' %] Váš účet vypršel. Prosím kontaktujte knihovnu pro další informace. [% ELSIF error == 'too_many' %] Vyčerpali jste možnosti prodlouežní. [% ELSIF error == 'on_reserve' %] Tato jednotka je rezervována jiným čtenářem. [% END %] [% END %] </span>
 </div>
 [% END %]

 [% IF ( patron_flagged ) %]
 <div class="alert">
 <ul>
 [% IF ( userdebarred ) %]
 <li id="userdebarred"><strong>Upozornění:</strong> Váš účet je zablokovaný[% IF ( BORROWER_INF.userdebarreddate ) %] do <span id="userdebarred_date">[% BORROWER_INF.userdebarreddate | $KohaDates %]</span>[% END %][% IF ( BORROWER_INF.debarredcomment ) %] s poznámkou <span id="userdebarred_comment">"[% BORROWER_INF.debarredcomment %]"</span>[% END %]. Obvyklým důvodem ke pozastavení výpůjček jsou neuhrazené poplatky. Pokud <a href="/cgi-bin/koha/opac-user.pl">stránka Vašeho účtu</a> neukazuje žádné problémy, poraďte se s pracovníkem knihovny.</li>
 [% END %]
 [% IF ( BORROWER_INF.gonenoaddress ) %]
 <li id="gonenoaddress"><strong>Upozornění:</strong> Podle našich záznamů nemáme vaše aktuální [% UNLESS ( BORROWER_INF.OPACPatronDetails ) %]<a href="/cgi-bin/koha/opac-userupdate.pl">kontaktní údaje</a>[% ELSE %]kontaktní údaje[% END %] v databázi. Prosím kontaktujte knihovnu[% IF ( BORROWER_INF.OPACPatronDetails ) %] nebo použijte <a href="/cgi-bin/koha/opac-userupdate.pl">on-line aktualizační formulář</a> pro odeslání aktuální informace (<em>Upozornění:</em> při odesílání online může dojít ke zpoždění při obnovování Vašeho účtu)[% END %].</li>
 [% END %]
 [% IF ( BORROWER_INF.lost ) %]
 <li id="lost"><strong>Upozornění: </strong> Vaše uživatelská průkazka byla označena jako ztracená nebo ukradená. Pokud tomu tak není, kontaktujte pracovníky knihovny.</li>
 [% END %]
 [% IF ( renewal_blocked_fines ) && ( OpacRenewalAllowed ) %]
 <li id="renewal_blocked_fines"><strong>Upozornění: </strong> Protože knihovně dlužíte na poplatcích <a href="/cgi-bin/koha/opac-account.pl">[% IF renewal_blocked_fines != "0.00" %] částku větší než <span id="renewal_blocked_fines_amount">[% renewal_blocked_fines %]</span> [% END %]</a>, nemůžete si prodloužit výpůjčky on-line. Prosím zaplaťte poplatek, pokud si chcete prodlužovat výpůjčky on-line.</li>
 [% END %]
 </ul>
 </div>
 [% END # / IF patron_flagged %]

 [% SET OPACMySummaryNote = Koha.Preference('OPACMySummaryNote') %]
 [% IF OPACMySummaryNote %][% OPACMySummaryNote %][% END %]

 <div id="opac-user-views" class="toptabs">
 <ul>
 <li><a href="#opac-user-checkouts">Vypůjčeno ([% issues_count %])</a></li>
 [% IF ( overdues_count ) %]<li><a href="#opac-user-overdues">Po termínu ([% overdues_count %])</a></li>[% END %]
 [% IF ( OPACFinesTab ) %]
 [% IF ( BORROWER_INF.amountoverfive ) %]<li><a href="#opac-user-fines">Poplatky ([% BORROWER_INF.amountoutstanding %])</a></li>[% END %]
 [% IF ( BORROWER_INF.amountoverzero ) %]<li><a href="#opac-user-fines">Poplatky ([% BORROWER_INF.amountoutstanding %])</a></li>[% END %]
 [% IF ( BORROWER_INF.amountlessthanzero ) %]<li><a href="#opac-user-fines">Kredit ([% BORROWER_INF.amountoutstanding %])</a></li>[% END %]
 [% END %]
 [% IF ( waiting_count ) %][% IF ( BORROWER_INF.atdestination ) %]<li><a href="#opac-user-waiting">K vyzvednutí ([% waiting_count %])</a></li>[% END %][% END %]
 [% IF ( reserves_count ) %]<li><a href="#opac-user-holds">Rezervace ([% reserves_count %])</a></li>[% END %]
 </ul>

 <div id="opac-user-checkouts">
 [% IF ( issues_count ) %]
 <form id="renewselected" action="/cgi-bin/koha/opac-renew.pl" method="post">
 <input type="hidden" name="borrowernumber" value="[% borrowernumber %]">
 <input type="hidden" name="from" value="opac_user" />
 <table id="checkoutst" class="table table-bordered table-striped">
 <caption>Počet aktuálních vypůjček: [% issues_count %]</caption>
 <thead>
 <tr>
 [% IF ( JacketImages ) %]<th class="nosort">&nbsp;</th>[% END %]
 <th class="anti-the">Název</th>
 <th class="title-string psort">Půjčeno do</th>
 [% UNLESS ( item_level_itypes ) %]
 <th>Typ jednotky</th>
 [% END %]
 [% IF ( show_barcode ) %]
 <th>Čárový kód</th>
 [% END %]
 <th>Signatura</th>
 [% IF ( OpacRenewalAllowed && !( borrower.is_expired && borrower.BlockExpiredPatronOpacActions ) ) %]
 <th class="nosort">Prodloužit</th>
 [% END %]
 [% IF ( OPACFinesTab ) %]
 <th>Poplatky</th>
 [% END %]
 [% IF ( OPACMySummaryHTML ) %]
 <th class="nosort">Odkazy</th>
 [% END %]
 </tr>
 </thead>
 <tbody>
 [% FOREACH ISSUE IN ISSUES %]
 [% IF ( ISSUE.overdue ) %]<tr class="overdue">[% ELSE %]<tr>[% END %]
 [% IF ( JacketImages ) %]<td class="jacketcell">

 [% IF ( OPACAmazonCoverImages ) %]
 [% IF ( ISSUE.normalized_isbn ) %]
 <a title="Zobrazit na Amazon.com" href="http://www.amazon.com/gp/reader/[% ISSUE.normalized_isbn %]/ref=sib_dp_pt/002-7879865-0184864#reader-link"><img alt="Zobrazit na Amazon.com" src="https://images-na.ssl-images-amazon.com/images/P/[% ISSUE.normalized_isbn %].01.THUMBZZZ.jpg" class="item-thumbnail" /></a>
 [% ELSE %]
 <a href="#"><span class="no-image">Obálka není dostupná</span></a>
 [% END %]
 [% END %]

 [% IF ( GoogleJackets ) %]
 [% IF ( ISSUE.normalized_isbn ) %]
 <div style="display:block;" id="gbs-thumbnail[% loop.count %]" class="[% ISSUE.normalized_isbn %]" title="Otevřít v Google Books"></div>
 [% ELSE %]
 <a href="http://books.google.com/books?q=[% ISSUE.title |url %]"><span class="no-image">Obálka není dostupná</span></a>
 [% END %]
 [% END %]

 [% IF ( BakerTaylorEnabled ) %]
 [% IF ( ISSUE.normalized_isbn ) %]
 <a href="https://[% BakerTaylorBookstoreURL |html %][% ISSUE.normalized_isbn %]"><img src="[% BakerTaylorImageURL |html %][% ISSUE.normalized_isbn %]" alt="Podívat se na Baker & Taylor" /></a>
 [% ELSE %]
 <span class="no-image">Obálka není dostupná</span><!-- BakerTaylor needs normalized_isbn! -->
 [% END %]
 [% END %]

 [% IF ( SyndeticsEnabled && SyndeticsCoverImages ) %]
 [% IF ( using_https ) %]
 <img src="https://secure.syndetics.com/index.aspx?isbn=[% ISSUE.normalized_isbn %]/SC.GIF&amp;client=[% SyndeticsClientCode %]&amp;type=xw10&amp;upc=[% ISSUE.normalized_upc %]&amp;oclc=[% ISSUE.normalized_oclc %]" alt="" class="item-thumbnail" />
 [% ELSE %]
 <img src="http://www.syndetics.com/index.aspx?isbn=[% ISSUE.normalized_isbn %]/SC.GIF&amp;client=[% SyndeticsClientCode %]&amp;type=xw10&amp;upc=[% ISSUE.normalized_upc %]&amp;oclc=[% ISSUE.normalized_oclc %]" alt="" class="item-thumbnail" />
 [% END %]
 [% END %]

 </td>[% END # / IF JacketImages %]

 <td class="title">
 <a class="title" href="/cgi-bin/koha/opac-detail.pl?biblionumber=[% ISSUE.biblionumber %]">[% ISSUE.title |html %] [% FOREACH subtitl IN ISSUE.subtitle %] [% subtitl.subfield %][% END %]</a>
 <span class="item-details">[% ISSUE.author %]</span>
 </td>
 [% IF ( ISSUE.overdue ) %]
 <td class="date_due overdue">
 <span title="[% ISSUE.date_due %]">
 <span class="tdlabel">Termín návratu:</span>
 [% ISSUE.date_due_sql | $KohaDates as_due_date => 1 %]
 </span>
 </td>
 [% ELSE %]
 <td class="date_due">
 <span title="[% ISSUE.date_due %]">
 <span class="tdlabel">Termín návratu:</span>
 [% ISSUE.date_due_sql | $KohaDates as_due_date => 1 %]
 </span>
 </td>
 [% END %]
 [% UNLESS ( item_level_itypes ) %]
 <td class="itype">
 <span class="tdlabel">Typ jednotky:</span>
 [% IF ( ISSUE.imageurl ) %]
 <img src="[% ISSUE.imageurl %]" title="[% ISSUE.description %]" alt="[% ISSUE.description %]" />
 [% END %] [% ISSUE.description %]
 </td>
 [% END %]
 [% IF ( show_barcode ) %]
 <td class="barcode">
 <span class="tdlabel">Čárový kód:</span>
 [% ISSUE.barcode %]
 </td>
 [% END %]
 <td class="call_no">
 <span class="tdlabel">Signatura:</span>
 [% ISSUE.itemcallnumber %]
 </td>
 [% IF ( OpacRenewalAllowed && !( borrower.is_expired && borrower.BlockExpiredPatronOpacActions ) ) %]
 <td class="renew">
 [% IF ISSUE.renewed %]<span class="blabel label-success">Prodlouženo!</span><br />[% END %]
 [% IF ( ISSUE.status ) %]
 [% IF ( canrenew ) %]
 <input type="checkbox" name="item" value="[% ISSUE.itemnumber %]"/> <a href="/cgi-bin/koha/opac-renew.pl?from=opac_user&amp;item=[% ISSUE.itemnumber %]&amp;borrowernumber=[% ISSUE.borrowernumber %]">Prodloužit</a>
 [% END %]
 <span class="renewals">(zbývá [% ISSUE.renewsleft %] z [% ISSUE.renewsallowed %] prodloužení)</span>
 [% ELSIF ( ISSUE.too_many ) %] Nelze prodloužit [% ELSIF ( ISSUE.auto_renew || ISSUE.auto_too_soon ) %] Automatické prodloužení <span class="renewals">(zbývá [% ISSUE.renewsleft %] z [% ISSUE.renewsallowed %] prodloužení)</span>
 [% ELSIF ( ISSUE.too_soon ) %] Nelze prodloužit před [% ISSUE.soonestrenewdate %] <span class="renewals">(zbývá [% ISSUE.renewsleft %] z [% ISSUE.renewsallowed %] prodloužení)</span>
 [% ELSIF ( ISSUE.on_reserve ) %]
 <span class="renewals">(Rezervováno)</span>
 [% END %]
 </td>
 [% END %]
 [% IF ( OPACFinesTab ) %]
 <td class="fines">
 <span class="tdlabel">Poplatky:</span>
 [% IF ( ISSUE.charges ) %] Ano [% ELSE %] Ne [% END %] </td>
 [% END %]
 [% IF ( OPACMySummaryHTML ) %]
 <td class="links">[% ISSUE.MySummaryHTML %]</td>
 [% END %]
 </tr>
 [% END # /FOREACH ISSUES %]
 </tbody>
 </table>
 [% IF ( canrenew && !userdebarred && OpacRenewalAllowed && !( borrower.is_expired && borrower.BlockExpiredPatronOpacActions ) ) %]
 <input class="btn" type="submit" value="Prodloužit vybrané" />
 [% END %]
 </form>

 [% IF ( canrenew && !userdebarred && OpacRenewalAllowed && !( borrower.is_expired && borrower.BlockExpiredPatronOpacActions ) ) %]
 <form id="renewall" action="/cgi-bin/koha/opac-renew.pl" method="post">
 <input type="hidden" name="from" value="opac_user" />
 <input type="hidden" name="borrowernumber" value="[% borrowernumber %]" />
 [% FOREACH ISSUE IN ISSUES %]
 <input type="hidden" name="item" value="[% ISSUE.itemnumber %]" />
 [% END %]
 <input value="Prodloužit vše" class="btn" type="submit" />
 </form>
 [% END %]
 [% ELSE %]
 <table class="table table-bordered table-striped">
 <tr><td>Nemáte nic vypůjčeného</td></tr>
 </table>
 [% END # IF issues_count %]
 </div> <!-- / .opac-user-checkouts -->

 [% IF ( OPACFinesTab ) %]
 <!-- FINES BOX -->
 [% IF ( BORROWER_INF.amountoverfive ) %]
 <div id="opac-user-fines"> <h3>Upomínky a poplatky</h3>
 <table class="table table-bordered table-striped">
 <thead><tr><th colspan="2">Částka (Kč)</th></tr></thead>
 <tbody>
 <tr>
 <td>Na poplatcích a upomínkách dlužíte částku:</td>
 <td><a href="/cgi-bin/koha/opac-account.pl">[% BORROWER_INF.amountoutstanding %]</a></td>
 </tr>
 </tbody>
 </table>
 </div>
 [% END %]

 [% IF ( BORROWER_INF.amountoverzero ) %]
 <div id="opac-user-fines"> <h3>Upomínky a poplatky</h3>
 <table class="table table-bordered table-striped">
 <thead><tr><th colspan="2">Částka (Kč)</th></tr></thead>
 <tbody>
 <tr>
 <td>Na poplatcích a upomínkách dlužíte částku:</td>
 <td><a href="/cgi-bin/koha/opac-account.pl">[% BORROWER_INF.amountoutstanding %]</a></td>
 </tr>
 </tbody>
 </table>
 </div>
 [% END %]

 [% IF ( BORROWER_INF.amountlessthanzero ) %]
 <div id="opac-user-fines"> <h3>Kredit</h3>
 <table class="table table-bordered table-striped">
 <thead><tr><th colspan="2">Částka (Kč)</th></tr></thead>
 <tbody>
 <tr>
 <td>Máte kredit v hodnotě:</td><td><a href="/cgi-bin/koha/opac-account.pl">[% BORROWER_INF.amountoutstanding %]</a></td>
 </tr>
 </tbody>
 </table>
 </div>
 [% END %]
 [% END # / OPACFinesTab %]

 [% END # / FOREACH BORROWER_INFO %]

 [% IF ( waiting_count && atdestination ) %]
 <div id="opac-user-waiting">
 <table id="waitingt" class="table table-bordered table-striped">
 <caption>Čekající rezervace</caption>
 <thead>
 <tr>
 <th class="anti-the">Název</th>
 <th>Datum rezervace</th>
 <th>Knihovna</th>
 </tr>
 </thead>
 <tbody>
 [% FOREACH WAITIN IN WAITING %]
 <tr>
 <td><img src="[% themelang %]/images/[% WAITIN.itemtype %].gif" alt="[% WAITIN.itemtype %]" title="[% WAITIN.itemtype %]" /></td>
 <td>
 <a class="title" href="opac-detail.pl?biblionumber=[% WAITIN.biblionumber %]">
 [% WAITIN.waiting_title %] [% FOREACH subtitl IN WAITIN.subtitle %] [% subtitl.subfield %][% END %]
 </a>
 <span class="item-details">
 [% WAITIN.author %]
 </span></td>
 <td>
 <span class="tdlabel">Datum rezervace:</span>
 [% WAITIN.reservedate | $KohaDates %]</td>
 <td>
 [% IF ( WAITIN.atdestination ) %]
 <strong>K vyzvednutí</strong> v [% WAITIN.branch %] [% ELSE %] Na cestě z [% WAITIN.holdingbranch %] do [% WAITIN.branch %] [% END %] </td>
 </tr>
 [% END %]
 </tbody>
 </table>
 </div> <!-- /#opac-user-waiting -->
 [% END # waiting_count && atdestination %]


 [% IF ( overdues_count ) %]
 <div id="opac-user-overdues">
 <table id="overduest" class="table table-bordered table-striped">
 <caption>Po termínu <span class="count">([% overdues_count %] celkem)</span></caption>
 <!-- OVERDUES TABLE ROWS -->
 <thead>
 <tr>
 [% IF ( JacketImages ) %]<th class="nosort">&nbsp;</th>[% END %]
 <th class="anti-the">Název</th>
 [% UNLESS ( item_level_itypes ) %]<th>Typ jednotky</th> [% END %]
 [% IF ( show_barcode ) %]<th>Čárový kód</th>[% END %]
 <th>Signatura</th>
 <th class="title-string psort">Půjčeno do</th>
 [% IF ( OpacRenewalAllowed ) %]
 <th class="nosort">Prodloužit</th>
 [% END %]
 [% IF ( OPACFinesTab ) %]
 <th>Poplatky</th>
 [% END %]
 </tr>
 </thead>
 <tbody>
 [% FOREACH OVERDUE IN OVERDUES %]
 <tr>
 [% IF ( JacketImages ) %]
 <td class="jacketcell">
 [% IF ( OPACAmazonCoverImages ) %]
 [% IF ( OVERDUE.normalized_isbn ) %]
 <a title="Zobrazit na Amazon.com" href="http://www.amazon.com/gp/reader/[% OVERDUE.normalized_isbn %]/ref=sib_dp_pt/002-7879865-0184864#reader-link"><img class="item-thumbnail" alt="Zobrazit na Amazon.com" src="https://images-na.ssl-images-amazon.com/images/P/[% OVERDUE.normalized_isbn %].01.THUMBZZZ.jpg" /></a>
 [% ELSE %]
 <a href="#"><span class="no-image">Obálka není dostupná</span></a>
 [% END %]
 [% END %]

 [% IF ( GoogleJackets ) %]
 [% IF ( OVERDUE.normalized_isbn ) %]
 <div style="display:block;" class="[% OVERDUE.normalized_isbn %]" id="gbs-thumbnail[% loop.count %]" title="Otevřít v Google Books"></div>
 [% ELSE %]
 <a href="http://books.google.com/books?q=[% OVERDUE.title |url %]"><span class="no-image">Obálka není dostupná</span></a>
 [% END %]
 [% END %]

 [% IF ( BakerTaylorEnabled ) %]
 [% IF ( OVERDUE.normalized_isbn ) %]
 <a href="https://[% BakerTaylorBookstoreURL |html %][% OVERDUE.normalized_isbn %]"><img src="[% BakerTaylorImageURL |html %][% OVERDUE.normalized_isbn %]" alt="Podívat se na Baker & Taylor" /></a>
 [% ELSE %]
 <!-- BakerTaylor needs normalized_isbn! --><span class="no-image">Obálka není dostupná</span>
 [% END %]
 [% END %]

 [% IF ( SyndeticsCoverImages ) %]
 [% IF ( using_https ) %]
 <img src="https://secure.syndetics.com/index.aspx?isbn=[% OVERDUE.normalized_isbn %]/SC.GIF&amp;client=[% SyndeticsClientCode %]&amp;upc=[% OVERDUE.normalized_upc %]&amp;oclc=[% OVERDUE.normalized_oclc %]&amp;type=xw10" alt="" class="item-thumbnail" />
 [% ELSE %]
 <img src="http://www.syndetics.com/index.aspx?isbn=[% OVERDUE.normalized_isbn %]/SC.GIF&amp;client=[% SyndeticsClientCode %]&amp;upc=[% OVERDUE.normalized_upc %]&amp;oclc=[% OVERDUE.normalized_oclc %]&amp;type=xw10" alt="" class="item-thumbnail" />
 [% END %]
 [% END %]
 </td>
 [% END # /IF jacketcell %]

 <td>
 <a class="title" href="/cgi-bin/koha/opac-detail.pl?bib=[% OVERDUE.biblionumber %]">[% OVERDUE.title |html %] [% FOREACH subtitl IN OVERDUE.subtitle %] [% subtitl.subfield %][% END %]
 </a>
 <span class="item-details">[% OVERDUE.author %]</span></td>

 [% UNLESS ( item_level_itypes ) %]
 <td>
 [% IF ( OVERDUE.imageurl ) %]
 <img src="[% OVERDUE.imageurl %]" title="[% OVERDUE.description %]" alt="[% OVERDUE.description %]" />
 [% END %] [% OVERDUE.description %]
 </td>
 [% END %]
 [% IF ( show_barcode ) %]
 <td>
 <span class="tdlabel">Čárový kód:</span>
 [% OVERDUE.barcode %]
 </td>
 [% END %]
 <td>
 <span class="tdlabel">Signatura:</span>
 [% OVERDUE.itemcallnumber %]
 </td>
 <td>
 <span title="[% OVERDUE.date_due %]">
 <span class="tdlabel">Termín návratu:</span>
 [% OVERDUE.date_due_sql | $KohaDates as_due_date => 1 %]
 </span>
 </td>
 [% IF ( OpacRenewalAllowed ) %]
 <td>
 [% IF ( OVERDUE.debarred ) %] Účet zablokován [% ELSIF ( OVERDUE.status ) %] [% IF ( canrenew ) %] <a href="/cgi-bin/koha/opac-renew.pl?from=opac_user&amp;item=[% OVERDUE.itemnumber %]&amp;bornum=[% OVERDUE.borrowernumber %]">Prodloužit</a>
 [% END %]
 <span class="renewals">(zbývá [% OVERDUE.renewsleft %] z [% OVERDUE.renewsallowed %] prodloužení)</span>
 [% ELSIF ( OVERDUE.onreserve ) %] Rezervováno [% ELSE %] Již jste vyčerpali maximální počet prodloužení [% END %] </td>
 [% END %]
 [% IF ( OPACFinesTab ) %]
 <td>
 <span class="tdlabel">Poplatky:</span>
 [% IF ( OVERDUE.charges ) %] Ano [% ELSE %] Ne [% END %] </td>
 [% END %]
 </tr>
 [% END %]
 </tbody>
 </table>
 </div> <!-- / #opac-user-overdues -->
 [% END # /overdues_count %]


 [% IF ( reserves_count ) %]
 <div id="opac-user-holds">
 <table id="holdst" class="table table-bordered table-striped">
 <caption>Rezervace <span class="count">([% reserves_count %] celkem)</span></caption>
 <!-- RESERVES TABLE ROWS -->
 <thead>
 <tr>
 <th class="anti-the">Název</th>
 <th class="psort">Zadáno v</th>
 <th>Vyprší</th>
 <th>Umístění</th>
 [% IF ( showpriority ) %]
 <th>Priorita</th>
 [% END %]
 <th>Stav</th>
 [% IF SuspendHoldsOpac %]
 <th class="nosort" >Odložit</th>
 [% END %]
 <th class="nosort">Změnit</th>
 </tr>
 </thead>
 <tbody>
 [% FOREACH RESERVE IN RESERVES %]
 [% IF ( RESERVE.wait ) %]
 [% IF ( RESERVE.atdestination ) %]
 [% IF ( RESERVE.found ) %]
 <tr class="reserved">
 [% ELSE %]
 <tr>
 [% END %]
 [% ELSE %]
 <tr class="transfered">
 [% END %]
 [% ELSE %]
 <tr>
 [% END %]
 <td class="title">
 <a class="title" href="/cgi-bin/koha/opac-detail.pl?biblionumber=[% RESERVE.biblionumber %]">
 [% RESERVE.reserves_title %]
 [% FOREACH subtitl IN RESERVE.subtitle %]
 [% subtitl.subfield %]
 [% END %]
 [% RESERVE.enumchron %]
 </a>
 [% RESERVE.author %]
 </td>
 <td class="reservedate">
 <span title="[% RESERVE.reservedate %]">
 <span class="tdlabel">Datum rezervace:</span>
 [% RESERVE.reservedate | $KohaDates %]
 </span>
 </td>
 <td class="expirationdate">
 [% IF ( RESERVE.expirationdate ) %]
 <span>
 <span class="tdlabel">Vypršení registrace:</span>
 [% RESERVE.expirationdate | $KohaDates %]
 </span>
 [% ELSE %]
 <span class="tdlabel">Vypršení registrace:</span>
 Nikdy [% END %] </td>
 <td class="branch">
 <span class="tdlabel">Vyzvednout v:</span>
 [% RESERVE.branch %]
 </td>
 [% IF ( showpriority ) %]
 <td class="priority">
 <span class="tdlabel">Priorita:</span>
 [% RESERVE.priority %]
 </td>
 [% END %]
 <td class="status">
 <span class="tdlabel">Stav:</span>
 [% IF ( RESERVE.wait ) %] [% IF ( RESERVE.atdestination ) %] [% IF ( RESERVE.found ) %] Čeká v <b> [% RESERVE.wbrname %]</b>[% IF ( RESERVE.waitingdate ) %] od [% RESERVE.waitingdate | $KohaDates %][% END %] <input type="hidden" name="pickup" value="[% RESERVE.wbrcd %]" />
 [% ELSE %] Jednotka čeká na vyzvednutí <b> [% RESERVE.wbrname %]</b>
 [% END %] [% ELSE %] Na cestě do <b> [% RESERVE.wbrname %]</b> <input type="hidden" name="pickup" value="[% RESERVE.wbrcd %]" />
 [% END %] [% ELSE %] [% IF ( RESERVE.intransit ) %] Na cestě z <b> [% RESERVE.frombranch %]</b> od [% RESERVE.datesent | $KohaDates %] [% ELSIF ( RESERVE.suspend ) %] Pozastaveno [% IF ( RESERVE.suspend_until ) %] do [% RESERVE.suspend_until %] [% END %] [% ELSE %] Zpracovává se [% END %] [% END %] </td>
 [% IF SuspendHoldsOpac %]
 <td>
 [% IF ( RESERVE.cancelable ) %]
 [% IF RESERVE.suspend %]
 <form class="form-inline" action="/cgi-bin/koha/opac-modrequest-suspend.pl" method="post">
 <input type="hidden" name="reserve_id" value="[% RESERVE.reserve_id %]" />
 <button class="btn btn-link" type="submit" name="submit"><i class="icon-play"></i> Pokračovat</button>
 </form>
 [% ELSE %]
 [% IF AutoResumeSuspendedHolds %]
 <a class="btn btn-link js-show" href="#suspendModal[% RESERVE.reserve_id %]" role="button" data-toggle="modal"><i class="icon-pause"></i> Odložit</a>
 [% # hold suspend modal form %]
 <div id="suspendModal[% RESERVE.reserve_id %]" class="modal-nojs" tabindex="-1" role="dialog" aria-labelledby="suspendModal[% RESERVE.reserve_id %]Label" aria-hidden="true">
 <form class="form-inline" action="/cgi-bin/koha/opac-modrequest-suspend.pl" method="post">
 <div class="modal-header">
 <button type="button" class="closebtn" data-dismiss="modal" aria-hidden="true">×</button>
 [% IF RESERVE.suspend %]
 <h3 id="suspendModal[% RESERVE.reserve_id %]Label">Pokračovat v rezervaci <i>[% RESERVE.reserves_title %]</i></h3>
 [% ELSE %]
 <h3 id="suspendModal[% RESERVE.reserve_id %]Label">Přerušit rezervaci <i>[% RESERVE.reserves_title %]</i></h3>
 [% END %]
 </div>
 <div class="modal-body">
 <input type="hidden" name="reserve_id" value="[% RESERVE.reserve_id %]" />
 <label for="suspend_until_[% RESERVE.reserve_id %]">Odložit do:</label>
 <input name="suspend_until" id="suspend_until_[% RESERVE.reserve_id %]" class="suspend-until" size="10" />
 [% INCLUDE 'date-format.inc' %]
 <p class="js-show"><a href="#" onclick="document.getElementById('suspend_until_[% RESERVE.reserve_id %]').value='';return false;">Smažte datum pro odložení na neurčito.</a></p>
 <button class="btn btn-mini js-hide" type="submit" name="submit">Odložit</button>
 </div>
 <div class="modal-footer">
 <button class="btn btn-primary" type="submit" name="submit">Odložit</button>
 <a href="#" data-dismiss="modal" aria-hidden="true" class="cancel">Zrušit</a>
 </div>
 </form>
 </div> <!-- /#suspendModal[% RESERVE.reserve_id %] -->
 [% ELSE %]
 <form class="form-inline" action="/cgi-bin/koha/opac-modrequest-suspend.pl" method="post">
 <input type="hidden" name="reserve_id" value="[% RESERVE.reserve_id %]" />
 <button class="btn btn-link" type="submit" name="submit"><i class="icon-pause"></i> Odložit</button>
 </form>
 [% END # / IF AutoResumeSuspendedHolds %]
 [% END # / IF RESERVE.suspend %]
 [% END # / IF ( RESERVE.cancelable )%]
 </td>
 [% END # / IF SuspendHoldsOpac %]
 <td class="modify">
 [% IF ( RESERVE.cancelable ) %]
 <form action="/cgi-bin/koha/opac-modrequest.pl" method="post">
 <input type="hidden" name="biblionumber" value="[% RESERVE.biblionumber %]" />
 <input type="hidden" name="reserve_id" value="[% RESERVE.reserve_id %]" />
 <button type="submit" name="submit" class="btn btn-mini btn-danger" onclick="return confirmDelete(MSG_CONFIRM_DELETE_HOLD);"><i class="icon-remove icon-white"></i> Zrušit</button></form>
 [% END %]
 </td>
 </tr>
 [% END # /FOREACH RESERVES %]
 </tbody>
 </table>

 [% IF SuspendHoldsOpac %]
 <div>
 <form class="form-inline" action="/cgi-bin/koha/opac-modrequest-suspend.pl" method="post">
 <button type="submit" name="submit" class="btn" onclick="return confirmDelete(MSG_CONFIRM_SUSPEND_HOLDS);"><i class="icon-pause"></i> Přerušit všechny rezervace</button>
 <input type="hidden" name="suspend" value="1" />

 [% IF AutoResumeSuspendedHolds %]
 <label for="suspend_until"> do </label>
 <input name="suspend_until" id="suspend_until" class="suspend-until" readonly="readonly" size="10" />
 <a href="#" style="font-size:85%;text-decoration:none;" onclick="document.getElementById('suspend_until').value='';return false;">Smažte datum pro odložení na neurčito.</a>
 [% END %]
 </form>
 </div>
 <br/>
 <div>
 <form action="/cgi-bin/koha/opac-modrequest-suspend.pl" method="post">
 <button type="submit" name="submit" class="btn" onclick="return confirmDelete(MSG_CONFIRM_RESUME_HOLDS);"><i class="icon-play"></i> Obnovit všechny přerušené rezervace</button>
 <input type="hidden" name="suspend" value="0" />
 </form>
 </div>
 [% END %]
 </div> <!-- / #opac-user-holds -->
 [% END # / #reserves_count %]
 </div> <!-- /#opac-user-views -->
 </div> <!-- /#userdetails -->
 </div> <!-- /.span10 -->
 </div> <!-- /.row-fluid -->
 </div> <!-- /.container-fluid -->
</div> <!-- /#main -->

[% INCLUDE 'opac-bottom.inc' %]


[% BLOCK jsinclude %]
 [% INCLUDE 'calendar.inc' %]
 [% INCLUDE 'datatables.inc' %]
 <script type="text/JavaScript">
        //<![CDATA[
        var MSG_CONFIRM_DELETE_HOLD   = _("Opravdu chce zrušit tuto rezervaci?");
        var MSG_CONFIRM_SUSPEND_HOLDS = _("Určitě chcete zrušit všechny rezervace?");
        var MSG_CONFIRM_RESUME_HOLDS  = _("Určitě chcete obnovit všechny zrušené rezervace?");

        $(document).ready(function(){
            $('#opac-user-views').tabs();
            $(".js-show").show();
            $(".js-hide").hide();
            $(".modal-nojs").addClass("modal").addClass("hide").removeClass("modal-nojs");
            $(".suspend-until").prop("readonly",1);
            var dTables = $("#checkoutst,#holdst,#overduest");
            dTables.each(function(){
                var thIndex = $(this).find("th.psort").index();
                $(this).dataTable($.extend(true, {}, dataTablesDefaults, {
                    "aaSorting" : [[ thIndex, 'asc' ]],
                    "aoColumnDefs": [
                        { "aTargets": [ "nosort" ],"bSortable": false,"bSearchable": false },
                        { "sType": "anti-the", "aTargets" : [ "anti-the" ] },
                        { "sType": "title-string", "aTargets" : [ "title-string" ] }
                    ]
                }));
            });

            [% IF ( GoogleJackets ) %]KOHA.Google.GetCoverFromIsbn();[% END %]
            [% IF ( OpacRenewalAllowed && canrenew && !userdebarred ) %]
                $("#renewselected").submit(function(){
                    valid = false;
                    $("input[type=checkbox]").each(function(){
                        if($(this).is(':checked')){
                            valid = true;
                        }
                    });
                    if(!valid){
                        alert(_("Nebyla vybrána žádná jednotka. Vyberte jednotky, které chcete prodloužit"));
                    }
                    return valid;
                });
                $("body").on("click","#renewselected_link",function(e){
                    e.preventDefault();
                    $("#renewselected").submit();
                });
                $("body").on("click","#renewall_link",function(e){
                    e.preventDefault();
                    $("#renewall").submit();
                });
                [% IF ( canrenew && !userdebarred && OpacRenewalAllowed && !( borrower.is_expired && borrower.BlockExpiredPatronOpacActions ) ) %]
                    $("#checkoutst caption").append("<div id=\"renewcontrols\"><a id=\"renewselected_link\" href=\"#\">"+_("Prodloužit vybrané")+"</a> <a id=\"renewall_link\" href=\"#\">"+_("Prodloužit vše")+"</a></div>");
                [% END %]
            [% END %]

            $( ".suspend-until" ).datepicker({ minDate: 1 }); // Require that "until date" be in the future
        });
        //]]>
    </script>
[% END %]
