<script type="text/javascript" src="[% interface %]/lib/jquery/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript">
//<![CDATA[
    var MSG_DT_FIRST = _("První");
    var MSG_DT_LAST = _("Poslední");
    var MSG_DT_NEXT = _("Další");
    var MSG_DT_PREVIOUS = _("Předchozí");
    var MSG_DT_EMPTY_TABLE = _("Tabulka neobsahuje žádné údaje");
    var MSG_DT_INFO = _("Zobrazuje se _START_ po _END_ z _TOTAL_");
    var MSG_DT_INFO_EMPTY = _("Žádné záznamy k zobrazení");
    var MSG_DT_INFO_FILTERED = _("(vybráno z _MAX_ celkových záznamů)");
    var MSG_DT_LENGTH_MENU = _("Zobrazit záznamy _MENU_");
    var MSG_DT_LOADING_RECORDS = _("Načítá se...");
    var MSG_DT_PROCESSING = _("Zpracovává se...");
    var MSG_DT_SEARCH = _("Hledat:");
    var MSG_DT_ZERO_RECORDS = _("Nebyly nalezeny žádné odpovídající záznamy");
    var CONFIG_EXCLUDE_ARTICLES_FROM_SORT = _("a an the");
//]]>
</script>
<script type="text/javascript" src="[% interface %]/[% theme %]/js/datatables.js"></script>