[% IF sort_by == "score asc" %]
 <option value="score asc" selected="selected">Relevance vzestupně</option>
[% ELSE %]
 <option value="score asc">Relevance vzestupně</option>
[% END %]
[% IF sort_by == "score desc" %]
 <option value="score desc" selected="selected">Relevance sestupně</option>
[% ELSE %]
 <option value="score desc">Relevance sestupně</option>
[% END %]

[% FOREACH ind IN sortable_indexes %]
 [% IF sort_by == "$ind.code asc" %]
 <option value="[% ind.code %] asc" selected="selected">[% ind.label %] vzestupně</option>
 [% ELSE %]
 <option value="[% ind.code %] asc">[% ind.label %] vzestupně</option>
 [% END %]
 [% IF sort_by == "$ind.code desc" %]
 <option value="[% ind.code %] desc" selected="selected">[% ind.label %] sestupně</option>
 [% ELSE %]
 <option value="[% ind.code %] desc">[% ind.label %] sestupně</option>
 [% END %]
[% END %]
