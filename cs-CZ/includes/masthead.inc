[% USE Koha %]
<div id="wrap">
 <div id="header-region" class="noprint">
 <div class="navbar navbar-inverse navbar-static-top">
 <div class="navbar-inner">
 <div class="container-fluid">
 <h1 id="logo">
 <a class="brand" href="/cgi-bin/koha/opac-main.pl">
 [% IF ( LibraryNameTitle ) %] [% LibraryNameTitle %] [% ELSE %] Koha on-line [% END %] </a>
 </h1>
 [% IF ( Koha.Preference( 'opacbookbag' ) == 1 ) %]
 <div id="cartDetails" class="cart-message">Váš košík je prázdný.</div>
 [% END %]
 <ul class="nav">
 [% IF ( Koha.Preference( 'opacbookbag' ) == 1 ) %]
 <li class="dropdown">
 <a title="Vyberte položky, které vás zajímají" class="dropdown-toggle" id="cartmenulink" data-toggle="dropdown" href="#" role="button">
 <i id="carticon" class="icon-shopping-cart icon-white"></i> <span class="cartlabel">Košík</span> <span id="basketcount"></span> <b class="caret"></b>
 </a>
 <ul aria-labelledby="cartmenulink" role="menu" class="dropdown-menu">
 <li role="presentation">
 <a href="#" id="cartmenuitem" class="cart-message" tabindex="-1" role="menuitem">Váš košík je prázdný.</a>
 </li>
 </ul>
 </li>
 [% END %]
 [% IF ( Koha.Preference( 'virtualshelves' ) == 1 ) && ( Koha.Preference( 'opacbookbag' ) == 1 ) %]
 <li class="divider-vertical"></li>
 [% END %]
 [% IF ( Koha.Preference( 'virtualshelves' ) == 1 ) %]
 <li class="dropdown">
 <a role="button" class="dropdown-toggle" href="#" data-toggle="dropdown" id="listsmenu" title="Zobrazit seznamy"><i class="icon-list icon-white"></i> <span class="listslabel">Seznamy</span> <b class="caret"></b></a>
 <ul aria-labelledby="listsmenu" role="menu" class="dropdown-menu">
 [% IF ( pubshelves ) %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?display=publicshelves" tabindex="-1" role="menuitem"><strong>Veřejné seznamy</strong></a></li>
 [% FOREACH pubshelvesloo IN pubshelvesloop %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?viewshelf=[% pubshelvesloo.shelfnumber %]&amp;sortfield=[% pubshelvesloo.sortfield %]" tabindex="-1" role="menuitem">[% pubshelvesloo.shelfname |html %]</a></li>
 [% END %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?display=publicshelves" tabindex="-1" role="menuitem" class="listmenulink">Zobrazit vše</a></li>
 [% ELSE %]
 <li role="presentation"><a href="#" tabindex="-1" class="menu-inactive" role="menuitem">Žádné veřejné seznamy.</a></li>
 [% END %]
 <li class="divider" role="presentation"></li>
 [% IF Koha.Preference( 'opacuserlogin' ) == 1 %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?display=privateshelves" tabindex="-1" role="menuitem"><strong>Vaše seznamy</strong></a></li>
 [% IF ( loggedinusername ) %]
 [% IF ( barshelves ) %]
 [% FOREACH barshelvesloo IN barshelvesloop %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?viewshelf=[% barshelvesloo.shelfnumber %]&amp;sortfield=[% barshelvesloo.sortfield %]" tabindex="-1" role="menuitem">[% barshelvesloo.shelfname |html %]</a></li>
 [% END %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?display=privateshelves" tabindex="-1" role="menuitem" class="listmenulink">Zobrazit vše</a></li>
 [% ELSE %]
 <li role="presentation"><a href="#" tabindex="-1" class="menu-inactive" role="menuitem">Žádné soukromé seznamy</a></li>
 <li role="presentation"><a href="/cgi-bin/koha/opac-shelves.pl?display=privateshelves" tabindex="-1" role="menuitem" class="listmenulink">Nový seznam</a></li>
 [% END %]
 [% ELSE %]
 <li role="presentation"><a href="/cgi-bin/koha/opac-user.pl" tabindex="-1" class="menu-inactive loginModal-trigger" role="menuitem">Pro vytvoření vlastního seznamu musíte být přihlášeni</a></li>
 [% END # / IF loggedinusername %]
 [% END # / IF opacuserlogin %]
 </ul> <!-- / .dropdown-menu -->
 </li> <!-- / .dropdown -->
 [% END # / IF virtualshelves %]
 </ul> <!-- / .nav -->
 [% IF Koha.Preference( 'virtualshelves' ) == 1 %]<div id="listsDetails"></div>[% END %]
 [% IF Koha.Preference( 'opacuserlogin' ) == 1 || EnableOpacSearchHistory %]
 <a id="user-menu-trigger" class="pull-right" href="#"><i class="icon-user"></i> <span class="caret"></span></a>
 <div id="members">
 <ul class="nav pull-right">
 [% IF Koha.Preference( 'opacuserlogin' ) == 1 %]
 [% UNLESS ( loggedinusername ) %]
 [% IF Koha.Preference('casAuthentication') %]
 [%# CAS authentication is too complicated for modal window %]
 <li><a href="/cgi-bin/koha/opac-user.pl">Přihlaste se do svého účtu</a></li>
 [% ELSE %]
 <li><a href="/cgi-bin/koha/opac-user.pl" class="loginModal-trigger" role="button" data-toggle="modal">Přihlaste se do svého účtu</a></li>
 [% END %]
 [% END %]
 [% IF ( loggedinusername ) %]
 <li><p class="members navbar-text">Vítejte, <a href="/cgi-bin/koha/opac-user.pl"><span class="loggedinusername">[% FOREACH USER_INF IN USER_INFO %][% USER_INF.title %] [% USER_INF.firstname %] [% USER_INF.surname %][% END %]</span></a></p></li>
 <li class="divider-vertical"></li>
 [% END %]
 [% END %]
 [% IF EnableOpacSearchHistory %]
 <li><p class="navbar-text"><a href="/cgi-bin/koha/opac-search-history.pl" title="Zobrazit Vaši historii hledání">Historie hledání</a> [<a class="logout" href="/cgi-bin/koha/opac-search-history.pl?action=delete" title="Vymazat historii hledání" onclick="return confirm(MSG_DELETE_SEARCH_HISTORY);">x</a>]</p></li>
 <li class="divider-vertical"></li>
 [% END %]
 [% IF Koha.Preference( 'opacuserlogin' ) == 1 %]
 [% IF ( loggedinusername ) %]
 <li><p class="navbar-text">
 [% IF persona %]
 <a class="logout" id="logout" href="/cgi-bin/koha/opac-main.pl?logout.x=1" onclick='navigator.id.logout();'>
 [% ELSE %]
 <a class="logout" id="logout" href="/cgi-bin/koha/opac-main.pl?logout.x=1">
 [% END %] Odhlásit se</a></p></li>
 [% END %]
 [% END %]
 </ul>
 </div> <!-- /members -->
 [% END # IF opacuserlogin %]
 </div> <!-- /container-fluid -->
 </div> <!-- /navbar-inner -->
 </div> <!-- /navbar -->

 [% IF ( opacheader ) %]
 <div class="container-fluid">
 <div class="row-fluid">
 <div id="opacheader">
 [% opacheader %]
 </div>
 </div>
 </div>
 [% END %]
 </div> <!-- / header-region -->

 <div class="container-fluid">
 <div class="row-fluid">
 <div id="opac-main-search">
 <!-- <div class="span2">
 [% IF ( opacsmallimage ) %]
 <h1 id="libraryname" style="background-image: url('[% opacsmallimage %]');">
 [% ELSE %]
 <h1 id="libraryname">
 [% END %]
 <a href="/cgi-bin/koha/opac-main.pl">[% IF ( LibraryName ) %][% LibraryName %][% ELSE %]Koha Online Catalog[% END %]</a></h1>
 </div> /.span2 -->
 <div class="span12">
 [% IF ( OpacPublic ) %]
 [% UNLESS ( advsearch ) %]
 [% IF ( Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 ) %]
 <div class="mastheadsearch librarypulldown">
 [% ELSE %]
 <div class="mastheadsearch">
 [% END %]
 [% IF Koha.Preference('OpacCustomSearch') == '' %]
 <form name="searchform" method="get" action="/cgi-bin/koha/opac-search.pl" id="searchform" class="form-inline">
 <label for="masthead_search"> Hledat [% UNLESS ( Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 ) %] [% IF ( mylibraryfirst ) %] (pouze v [% mylibraryfirst %]) [% END %] [% END %] </label>

 <select name="idx" id="masthead_search">
 [% IF ( ms_kw ) %]
 <option selected="selected" value="">Katalog knihovny</option>
 [% ELSE %]
 <option value="">Katalog knihovny</option>
 [% END # /ms_kw %]
 [% IF ( ms_ti ) %]
 <option selected="selected" value="ti">Název</option>
 [% ELSE %]
 <option value="ti">Název</option>
 [% END # /ms_ti %]
 [% IF ( ms_au ) %]
 <option selected="selected" value="au">Autor</option>
 [% ELSE %]
 <option value="au">Autor</option>
 [% END # /ms_au%]
 [% IF ( ms_su ) %]
 <option selected="selected" value="su">Předmětové heslo</option>
 [% ELSE %]
 <option value="su">Předmětové heslo</option>
 [% END # /ms_su %]
 [% IF ( ms_nb ) %]
 <option selected="selected" value="nb">ISBN</option>
 [% ELSE %]
 <option value="nb">ISBN</option>
 [% END # /ms_nb%]
 [% IF ( ms_se ) %]
 <option selected="selected" value="se">Edice</option>
 [% ELSE %]
 <option value="se">Edice</option>
 [% END # /ms_se %]
 [% IF ( numbersphr ) %]
 [% IF ( ms_callnum ) %]
 <option selected="selected" value="callnum,phr">Signatura</option>
 [% ELSE %]
 <option value="callnum,phr">Signatura</option>
 [% END #/ms_callnum %]
 [% ELSE %]
 [% IF ( ms_callnum ) %]
 <option selected="selected" value="callnum">Signatura</option>
 [% ELSE %]
 <option value="callnum">Signatura</option>
 [% END # /ms_callnum %]
 [% END # /numbersphr %]
 </select>

 [% UNLESS ( Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 ) %]
 <div class="input-append nolibrarypulldown">
 [% END %]
 [% IF ( ms_value ) %]
 <input value="[% ms_value |html %]" class="transl1" type="text" id="translControl1" title="Vložte hledaný výraz" name="q" /><span id="translControl"></span>
 [% ELSE %]
 <input title="Vložte hledaný výraz" name="q" class="transl1" type="text" id="translControl1" /><span id="translControl"></span>
 [% END # /ms_value %]

 [% UNLESS ( Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 ) %]
 <button type="submit" id="searchsubmit" class="btn btn-primary">OK</button>
 </div>
 [% END %]

 [% IF ( Koha.Preference( 'OpacAddMastheadLibraryPulldown' ) == 1 ) %]
 <div class="input-append">
 <select name="branch_group_limit" id="select_library">
 <option value="">Všechny knihovny</option>
 [% IF BranchCategoriesLoop %]<optgroup label="Knihovny">[% END %]
 [% FOREACH BranchesLoo IN BranchesLoop %]
 [% IF ( BranchesLoo.selected ) %]<option selected="selected" value="branch:[% BranchesLoo.value %]">[% BranchesLoo.branchname %]</option>
 [% ELSE %]<option value="branch:[% BranchesLoo.value %]">[% BranchesLoo.branchname %]</option>[% END %]
 [% END %]
 [% IF BranchCategoriesLoop %]
 </optgroup>
 <optgroup label="Skupiny">
 [% FOREACH bc IN BranchCategoriesLoop %]
 [% IF ( bc.selected ) %]
 <option selected="selected" value="multibranchlimit-[% bc.categorycode %]">[% bc.categoryname %]</option>
 [% ELSE %]
 <option value="multibranchlimit-[% bc.categorycode %]">[% bc.categoryname %]</option>
 [% END # / bc.selected %]
 [% END %]
 </optgroup>
 [% END # / BranchCategoriesLoop %]
 </select>
 <button type="submit" id="searchsubmit" class="btn btn-primary">OK</button>
 </div>
 [% ELSE %]
 [% IF ( opac_limit_override ) %]
 [% IF ( opac_search_limit ) %]
 <input name="limit" value="[% opac_search_limit %]" type="hidden" />
 [% END %]
 [% ELSE %]
 [% IF ( mylibraryfirst ) %]
 <input name="limit" value="branch:[% mylibraryfirst %]" type="hidden" />
 [% END %]
 [% END # / opac_limit_override %]
 [% END # / OpacAddMastheadLibraryPulldown %]

 </form>
 [% ELSE # / Koha.Preference('OpacCustomSearch') == '' %]
 [% Koha.Preference('OpacCustomSearch') %]
 [% END # / Koha.Preference('OpacCustomSearch') == '' %]
 </div> <!-- / .mastheadsearch -->
 [% END # / UNLESS advsearch %]

 <div class="row-fluid">
 <div id="moresearches">
 <ul>
 <li><a href="/cgi-bin/koha/opac-search.pl">Pokročilé vyhledávání</a></li>
 [% IF ( Koha.Preference( 'UseCourseReserves' ) == 1 ) %]<li><a href="/cgi-bin/koha/opac-course-reserves.pl">Kurzy</a></li>[% END %]
 [% IF Koha.Preference( 'OpacBrowser' ) == 1 %]<li><a href="/cgi-bin/koha/opac-browser.pl">Procházet podle hierarchie</a></li>[% END %]
 [% IF Koha.Preference( 'OpacAuthorities' ) == 1 %]<li><a href="/cgi-bin/koha/opac-authorities-home.pl">Vyhledávání autority</a></li>[% END %]
 [% IF ( ( Koha.Preference( 'opacuserlogin' ) == 1 ) && ( Koha.Preference( 'reviewson' ) == 1 ) && ( Koha.Preference( 'OpacShowRecentComments' ) == 1 ) ) %]<li><a href="/cgi-bin/koha/opac-showreviews.pl">Nejnovější komentáře</a></li>[% END %]
 [% IF Koha.Preference( 'TagsEnabled' ) == 1 %]<li><a href="/cgi-bin/koha/opac-tags.pl">Oblak štítků</a></li>[% END %]
 [% IF Koha.Preference( 'OpacCloud' ) == 1 %]<li><a href="/cgi-bin/koha/opac-tags_subject.pl">Nejčastější témata</a></li>[% END %]
 [% IF Koha.Preference( 'OpacTopissue' ) == 1 %]<li><a href="/cgi-bin/koha/opac-topissues.pl">Nejoblíbenější</a></li>[% END %]
 [% IF Koha.Preference( 'suggestion' ) == 1 %]
 [% IF Koha.Preference( 'AnonSuggestions' ) == 1 %]
 <li><a href="/cgi-bin/koha/opac-suggestions.pl">Návrhy na nákup</a></li>
 [% ELSIF ( ( Koha.Preference( 'OPACViewOthersSuggestions' ) == 1 ) ) %]
 <li><a href="/cgi-bin/koha/opac-suggestions.pl">Návrhy na nákup</a></li>
 [% END %]
 [% END %]
 </ul>
 </div> <!-- /#moresearches -->
 </div> <!-- /.row-fluid -->

 [% END # / OpacPublic %]
 </div> <!-- /.span10 -->
 </div> <!-- /.opac-main-search -->
 </div> <!-- / .row-fluid -->
 </div> <!-- /.container-fluid -->

 <!-- Login form hidden by default, used for modal window -->
 <div id="loginModal" class="modal hide" tabindex="-1" role="dialog" aria-labelledby="modalLoginLabel" aria-hidden="true">
 <div class="modal-header">
 <button type="button" class="closebtn" data-dismiss="modal" aria-hidden="true">×</button>
 <h3 id="modalLoginLabel">Přihlaste se do svého účtu</h3>
 </div>
 <form action="/cgi-bin/koha/opac-user.pl" method="post" name="auth" id="modalAuth">
 <div class="modal-body">
 [% IF ( shibbolethAuthentication ) %]
 [% IF ( invalidShibLogin ) %]
 <!-- This is what is displayed if shibboleth login has failed to match a koha user -->
 <div class="alert alert-info">
 <p>Omlouváme se, ale vaše Shibbolethová identita neodpovídá záznamu v knihovně. Pokud máte místní účet, můžete jej použít níže.</p>
 </div>
 [% ELSE %]
 <h4>Přihlášení přes Shibboleth</h4>
 <p>Pokud mátě účet Shibboleth, prosím <a href="[% shibbolethLoginUrl %]">klikněte zde pro přihlášení</a>.</p>
 <h4>Lokální přihlášení</h4>
 [% END %]
 [% END %]
 <input type="hidden" name="koha_login_context" value="opac" />
 <fieldset class="brief">
 <label for="muserid">Přihlašovací jméno:</label><input type="text" id="muserid" name="userid" />
 <label for="mpassword">Heslo:</label><input type="password" id="mpassword" name="password" />
 [% IF Koha.Preference( 'NoLoginInstructions' ) %]
 <div id="nologininstructions-modal">
 [% Koha.Preference( 'NoLoginInstructions' ) %]
 </div>
 [% END %]
 [% IF PatronSelfRegistration && PatronSelfRegistrationDefaultCategory %]<div id="mpatronregistration"><p>Nemáte ještě účet v knihovně? <a href="/cgi-bin/koha/opac-memberentry.pl">Zaregistrujte se.</a></p></div>[% END %]
 </fieldset>
 </div>
 <div class="modal-footer">
 <input class="btn btn-primary" type="submit" value="Přihlaste se" />
 <a href="#" data-dismiss="modal" aria-hidden="true" class="cancel">Zrušit</a>
 </div>
 </form> <!-- /#auth -->
 </div> <!-- /#modalAuth -->
