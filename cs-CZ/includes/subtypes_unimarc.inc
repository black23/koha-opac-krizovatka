<fieldset>
 <legend>Kódovaná pole</legend>
 <p>
 <label>Uživatelské určení</label>
 <select name="limit" class="subtype">
 <option value="" selected="selected" >Libovolné</option>
 <option value="aud:a">děti a mládež obecně</option>
 <option value="aud:b">předškoláci (0-5)</option>
 <option value="aud:c">mladší žáci (5-8)</option>
 <option value="aud:d">děti (9-14)</option>
 <option value="aud:e">mládež</option>
 <option value="aud:k">dospělí, vážná</option>
 <option value="aud:m">dospělí, obecné</option>
 <option value="aud:u">neznámé</option>
 </select>
 </p>
 <p>
 <label>Fyzická prezentace</label>
 <select name="limit" class="subtype">
 <option value="" selected="selected" >Libovolné</option>
 <option value="Material-type:r">běžný tisk</option>
 <option value="Material-type:d">velkoplošný tisk</option>
 <option value="Material-type:e">formát novin</option>
 <option value="Material-type:f">Braillovo nebo Moon písmo</option>
 <option value="Material-type:g">mikrotisk</option>
 <option value="Material-type:h">ručně psaný</option>
 <option value="Material-type:i">multimédia</option>
 <option value="Material-type:j">mini-výtisk</option>
 <option value="Material-type:s">elektronický zdroj</option>
 <option value="Material-type:t">mikroformát</option>
 <option value="Material-type:z">jiný druh textového materiálu</option>
 </select>
 </p>
 <p>
 <label>Literární žánr</label>
 <select name="limit" class="subtype">
 <option value="" selected="selected" >Libovolné</option>
 <option value="Literature-Code:a">beletrie</option>
 <option value="Literature-Code:b">drama</option>
 <option value="Literature-Code:c">eseje</option>
 <option value="Literature-Code:d">humor, satira</option>
 <option value="Literature-Code:e">dopisy</option>
 <option value="Literature-Code:f">krátké příběhy</option>
 <option value="Literature-Code:g">poezie</option>
 <option value="Literature-Code:h">projevy, řečnictví</option>
 <option value="Literature-Code:i">libreta</option>
 <option value="Literature-Code:y">ne literární text</option>
 <option value="Literature-Code:z">jiné literární formy (a kombinace forem)</option>
 </select>
 </p>
 <p>
 <label>Biografie</label>
 <select name="limit" class="subtype" size="1">
 <option value="">Libovolné</option>
 <option value="Biography-code:y">není životopis</option>
 <option value="Biography-code:a">vlastní životopis</option>
 <option value="Biography-code:b">individuální životopis</option>
 <option value="Biography-code:c">společný životopis</option>
 <option value="Biography-code:d">obsahuje životopisné údaje</option>
 </select>
 </p>
 <p>
 <label>Ilustrace</label>
 <select name="limit" class="subtype" size="1">
 <option value="">Libovolné</option>
 <option value="Illustration-Code:a">ilustrace</option>
 <option value="Illustration-Code:b">mapy</option>
 <option value="Illustration-Code:c">portréty</option>
 <option value="Illustration-Code:d">grafy</option>
 <option value="Illustration-Code:e">plány</option>
 <option value="Illustration-Code:f">desky</option>
 <option value="Illustration-Code:g">hudba</option>
 <option value="Illustration-Code:h">faksimile</option>
 <option value="Illustration-Code:i">erby</option>
 <option value="Illustration-Code:j">genealogické tabulky</option>
 <option value="Illustration-Code:k">formuláře</option>
 <option value="Illustration-Code:l">ukázky</option>
 <option value="Illustration-Code:m">zvukové nahrávky</option>
 <option value="Illustration-Code:n">fólie</option>
 <option value="Illustration-Code:o">iluminace</option>
 <option value="Illustration-Code:y">bez ilustrací</option>
 </select>
 </p>
 <p>
 <label>Obsah</label>
 <select name="limit" class="subtype">
 <option value="" >Libovolné</option>
 <option value="ctype:a" >bibliografie</option>
 <option value="ctype:b" >katalog</option>
 <option value="ctype:c" >index</option>
 <option value="ctype:d" >abstrakt</option>
 <option value="ctype:e" >slovník</option>
 <option value="ctype:f" >encyklopedie</option>
 <option value="ctype:g" >adresář</option>
 <option value="ctype:h" >popis projektu</option>
 <option value="ctype:i" >statistika</option>
 <option value="ctype:j" >učebnice</option>
 <option value="ctype:k" >patent</option>
 <option value="ctype:l" >standard</option>
 <option value="ctype:m" >disertační nebo diplomová práce</option>
 <option value="ctype:n" >právo</option>
 <option value="ctype:o" >číselná tabulka</option>
 <option value="ctype:p" >technická zpráva</option>
 <option value="ctype:q" >výzkumná zpráva</option>
 <option value="ctype:r" >literární ankety/recenze</option>
 <option value="ctype:s" >smlouvy</option>
 <option value="ctype:t" >komiks</option>
 <option value="ctype:v" >disertační nebo diplomová práce</option>
 <option value="ctype:w" >náboženský text</option>
 <option value="ctype:z" >jiné</option>
 </select>
 </p>
 <p>
 <label>Typ videa</label>
 <select name="limit" class="subtype">
 <option value="">Libovolné</option>
 <option value="Video-mt:a">film</option>
 <option value="Video-mt:b">vizuální projekce</option>
 <option value="Video-mt:c">videozáznam</option>
 </select>
 </p>
</fieldset>

<fieldset>
 <legend>Periodika</legend><p>
 <p>
 <label>Typ periodika</label>
 <select name="limit" class="subtype">
 <option value="">Jakýkoliv typ</option>
 <option value="Type-Of-Serial:a">Periodický</option>
 <option value="Type-Of-Serial:b">Knižní edice</option>
 <option value="Type-Of-Serial:c">Noviny</option>
 <option value="Type-Of-Serial:e">Volné listy</option>
 <option value="Type-Of-Serial:f">Databáze</option>
 <option value="Type-Of-Serial:g">Aktualizované webové stránky</option>
 <option value="Type-Of-Serial:z">Jiný</option>
 </select>
 </p>
 <p>
 <label>Četnost vydání</label>
 <select name="limit" class="subtype">
 <option value="">Libovolné</option>
 <option value="Frequency-code:a">Denně</option>
 <option value="Frequency-code:b">Dvakrát týdně</option>
 <option value="Frequency-code:c">Jednou týdně</option>
 <option value="Frequency-code:d">Jednou za dva týdny</option>
 <option value="Frequency-code:e">Dvakrát měsíčně</option>
 <option value="Frequency-code:f">Měsíčně</option>
 <option value="Frequency-code:g">Jednou za dva měsíce</option>
 <option value="Frequency-code:h">Čtvrtletně</option>
 <option value="Frequency-code:i">Třikrát ročně</option>
 <option value="Frequency-code:j">Dvakrát ročně</option>
 <option value="Frequency-code:k">Roční</option>
 <option value="Frequency-code:l">Jednou za dva roky</option>
 <option value="Frequency-code:m">Jednou za tři roky</option>
 <option value="Frequency-code:n">Třikrát týdně</option>
 <option value="Frequency-code:o">Třikrát měsíčně</option>
 <option value="Frequency-code:y">Neurčená</option>
 <option value="Frequency-code:u">Neznámé</option>
 <option value="Frequency-code:z">Jiný</option>
 </select>
 </p>
 <p>
 <label>Pravidelnost</label>
 <select name="limit" class="subtype">
 <option value="">Jakákoliv pravidelnost</option>
 <option value="Regularity-code:a">pravidelný</option>
 <option value="Regularity-code:b">pravidelně s výjimkami</option>
 <option value="Regularity-code:y">nepravidelně</option>
 <option value="Regularity-code:u">neznámé</option>
 </select>
 </p>
</fieldset>

<fieldset>
 <legend>Obrázek</legend>
 <select name="limit" class="subtype">
 <option value="">Libovolné</option>
 <option value="Graphics-type:a">koláž</option>
 <option value="Graphics-type:b">kresba</option>
 <option value="Graphics-type:c">malba</option>
 <option value="Graphics-type:d">fotomechanická reprodukce</option>
 <option value="Graphics-type:e">fotonegativ</option>
 <option value="Graphics-type:f">fototisk</option>
 <option value="Graphics-type:h">obrázek</option>
 <option value="Graphics-type:i">tisk</option>
 <option value="Graphics-type:k">technický nákres</option>
 <option value="Graphics-type:z">jiná grafika než projekce</option>
 </select>
 <select name="limit" class="subtype">
 <option value="">Libovolné</option>
 <option value="Graphics-support:a">plátno</option>
 <option value="Graphics-support:b">bristolský papír</option>
 <option value="Graphics-support:c">karton</option>
 <option value="Graphics-support:d">sklo</option>
 <option value="Graphics-support:j">omítka</option>
 <option value="Graphics-support:k">dřevotříska</option>
 <option value="Graphics-support:l">porcelán</option>
 <option value="Graphics-support:m">kámen</option>
 <option value="Graphics-support:n">dřevo</option>
 <option value="Graphics-support:v">smíšená sbírka</option>
 <option value="Graphics-support:e">umělá hmota</option>
 <option value="Graphics-support:f">kůže</option>
 <option value="Graphics-support:g">textil</option>
 <option value="Graphics-support:h">kov</option>
 <option value="Graphics-support:i">papír</option>
 <option value="Graphics-support:z">ostatní</option>
 <option value="Graphics-support:u">neznámé</option>
 </select>
</fieldset>
