[% UNLESS ( is_popup ) %]
 [% IF ( opaccredits ) %]
 <div class="container-fluid">
 <div class="row-fluid">
 <div class="span12">
 <div id="opaccredits" class="noprint">
 [% opaccredits %]
 </div>
 </div>
 </div>
 </div>
 [% END #/ opaccredits %]

 [% IF ( OpacKohaUrl ) %]
 <div class="container-fluid">
 <div class="row-fluid">
 <div class="span12">
 <div id="koha_url" class="clearfix noprint">
 <p>Používáme knihovní systém [% IF template.name.match('opac-main.tt') %] <a class="koha_url" href="http://koha-community.org">Koha</a>
 [% ELSE %]
 <a class="koha_url" rel="nofollow" href="http://koha-community.org">Koha</a>
 [% END %]</p>
 </div>
 </div> <!-- /.span12 -->
 </div> <!-- /.row-fluid -->
 </div> <!-- /.container-fluid -->
 [% END # / OpacKohaUrl %]

</div> <!-- / #wrap in masthead.inc -->

 [% IF ( opaclanguagesdisplay ) %]
 [% IF ( languages_loop && opaclanguagesdisplay ) %]
 [% UNLESS ( one_language_enabled ) %]
 <div id="changelanguage" class="navbar navbar-fixed-bottom navbar-static-bottom noprint">
 <div class="navbar-inner">
 <ul id="i18nMenu" class="nav">
 <li><p class="lang navbar-text"><strong>Jazyky:&nbsp;</strong></p></li>
 [% FOREACH languages_loo IN languages_loop %]
 [% IF ( languages_loo.group_enabled ) %]
 [% IF ( languages_loo.plural ) %]
 <li class="dropdown">
 <a data-toggle="dropdown" class="dropdown-toggle sublangs" id="show[% languages_loo.rfc4646_subtag %]" href="#">[% IF ( languages_loo.native_description ) %][% languages_loo.native_description %][% ELSE %][% languages_loo.rfc4646_subtag %][% END %] <b class="caret"></b></a>
 <ul id="sub[% languages_loo.rfc4646_subtag %]" class="dropdown-menu">
 [% FOREACH sublanguages_loo IN languages_loo.sublanguages_loop %]
 [% IF ( sublanguages_loo.enabled ) %]
 [% IF ( sublanguages_loo.sublanguage_current ) %]
 <li> <p>[% sublanguages_loo.native_description %] [% sublanguages_loo.script_description %] [% sublanguages_loo.region_description %] [% sublanguages_loo.variant_description %] ([% sublanguages_loo.rfc4646_subtag %])</p></li>
 [% ELSE %]
 <li><a href="/cgi-bin/koha/opac-changelanguage.pl?language=[% sublanguages_loo.rfc4646_subtag %]"> [% sublanguages_loo.native_description %] [% sublanguages_loo.script_description %] [% sublanguages_loo.region_description %] [% sublanguages_loo.variant_description %] ([% sublanguages_loo.rfc4646_subtag %])</a></li>
 [% END %]
 [% END # / IF sublanguages_loo.enabled %]
 [% END # / FOREACH sublanguages_loo %]
 </ul>
 </li> <!-- / .more -->
 [% ELSE %]
 [% IF ( languages_loo.group_enabled ) %]
 [% IF ( languages_loo.current ) %]
 <li class="active"><p class="navbar-text">[% IF ( languages_loo.native_description ) %][% languages_loo.native_description %][% ELSE %][% languages_loo.rfc4646_subtag %][% END %]</p></li>
 [% ELSE %]
 <li><a href="/cgi-bin/koha/opac-changelanguage.pl?language=[% languages_loo.rfc4646_subtag %]">[% IF ( languages_loo.native_description ) %][% languages_loo.native_description %][% ELSE %][% languages_loo.rfc4646_subtag %][% END %]</a></li>
 [% END %]
 [% END # / IF languages_loo.current %]
 [% END # / IF ( languages_loo.plural ) %]
 [% END # / IF ( languages_loo.group_enabled ) %]
 [% END # / FOREACH languages_loo IN languages_loop %]
 </ul> <!-- / #i18menu -->
 </div> <!-- / .navbar-inner -->
 </div> <!-- / #changelanguage -->
 [% END # / UNLESS ( one_language_enabled ) %]
 [% END # / IF ( languages_loop && opaclanguagesdisplay ) %]
 [% END # / IF opaclanguagesdisplay %]
[% END # / UNLESS is_popup %]


<!-- JavaScript includes -->
<script type="text/javascript" src="[% interface %]/[% theme %]/lib/jquery/jquery.js"></script>
<script type="text/javascript" src="[% interface %]/[% theme %]/lib/jquery/jquery-ui.js"></script>
<script type="text/javascript">
// Resolve name collision between jQuery UI and Twitter Bootstrap
$.widget.bridge('uitooltip', $.ui.tooltip);
</script>
<script type="text/javascript" src="[% interface %]/[% theme %]/lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="[% interface %]/[% theme %]/js/global.js"></script>
<script type="text/javascript">
    Modernizr.load([
        // Test need for polyfill
        {
            test: window.matchMedia,
            nope: "[% interface %]/[% theme %]/lib/media.match.min.js"
        },
        // and then load enquire
        "[% interface %]/[% theme %]/lib/enquire.min.js",
        "[% interface %]/[% theme %]/js/script.js"
    ]);
</script>
[% IF ( OPACAmazonCoverImages || SyndeticsCoverImages ) %]
<script type="text/javascript">//<![CDATA[
        var NO_AMAZON_IMAGE = _("Obálka není dostupná");
    //]]>
</script>
<script type="text/javascript" src="[% interface %]/[% theme %]/js/amazonimages.js"></script>
[% END %]

<script type="text/javascript">
    //<![CDATA[
    var MSG_CONFIRM_AGAIN = _("Varování: Tato akce je nevratná. Opravdu mám pokračovat?")
    var MSG_DELETE_SEARCH_HISTORY = _("Opravdu chcete vymazat vaši historii prohlížení?");
    var MSG_NO_SUGGESTION_SELECTED = _("Nebyly vybrány žádné návrhy");
    [% IF Koha.Preference( 'opacbookbag' ) == 1 or Koha.Preference( 'virtualshelves' ) == 1 %]
        var MSG_BASKET_EMPTY = _("Váš košík je prázdný");
        var MSG_RECORD_IN_BASKET = _("Tuto jednotku už v košíku máte");
        var MSG_RECORD_ADDED = _("Jednotka byla vložena do košíku");
        var MSG_RECORD_REMOVED = _("Jednotka byla vyjmuta z košíku");
        var MSG_NRECORDS_ADDED = _(" položka/y vložena/y do košíku");
        var MSG_NRECORDS_IN_BASKET = _("již jsou v košíku");
        var MSG_NO_RECORD_SELECTED = _("Nebyly vybrány žádné položky");
        var MSG_NO_RECORD_ADDED = _("Do košíku nebyly vloženy žádné položky");
        var MSG_CONFIRM_DEL_BASKET = _("Opravdu chcete odstranit z košíku všechny položky?");
        var MSG_CONFIRM_DEL_RECORDS = _("Opravdu chcete odstranit vybrané položky?");
        var MSG_ITEM_IN_CART = _("V košíku");
        var MSG_IN_YOUR_CART = _("Položky ve Vašem košíku: ");
        var MSG_ITEM_NOT_IN_CART = _("Přidat do košíku");
        var MSG_NO_RECORD_SELECTED = _("Nebyly vybrány žádné položky");
    [% END %]
    [% IF ( Koha.Preference( 'opacuserlogin' ) == 1 ) && ( Koha.Preference( 'TagsEnabled' ) == 1 ) %]
        var MSG_TAGS_DISABLED = _("Litujeme, štítky nejsou v tomto systému povolené.");
        var MSG_TAG_ALL_BAD = _("Chyba! Váš tag se skládá pouze ze značkovacího kódu, proto NEBYL uložen. Zadejte pouze čistý text a zkuste to znovu.");
        var MSG_ILLEGAL_PARAMETER = _("Chyba! Neplatný parametr");
        var MSG_TAG_SCRUBBED = _("Poznámka: štítek obsahoval značkovací kód, který byl odstraněn. Výsledný štítek je ");
        var MSG_ADD_TAG_FAILED = _("Chyba! Nepodařilo se přidat štítek");
        var MSG_ADD_TAG_FAILED_NOTE = _("Poznámka: každý štítek můžete k jednomu záznamu přiřadit jen jednou. K zobrazení svých štítků použijte záložku 'Štítky' v uživatelském účtu.");
        var MSG_DELETE_TAG_FAILED = _("Chyba! Nelze odstranit štítek");
        var MSG_DELETE_TAG_FAILED_NOTE = _("Poznámka: mazat můžete pouze vámi vložené štítky.")
        var MSG_LOGIN_REQUIRED = _("Pro přidávání štítků musíte být přihlášeni.");
        var MSG_TAGS_ADDED = _("Přidané štítky: ");
        var MSG_TAGS_DELETED = _("Přidané štítky: ");
        var MSG_TAGS_ERRORS = _("Chyby: ");
        var MSG_MULTI_ADD_TAG_FAILED = _("Jeden nebo více štítků nebylo možné přidat.");
        var MSG_NO_TAG_SPECIFIED = _("Žádný štítek nebyl zadán.");
    [% END %]
    [% IF ( OPACAmazonCoverImages || SyndeticsCoverImages ) %]
        $(window).load(function() {
            verify_images();
         });
    [% END %]
    //]]>
</script>

[% IF Koha.Preference( 'opacbookbag' ) == 1 %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/basket.js"></script>
[% ELSIF ( Koha.Preference( 'virtualshelves' ) == 1 ) %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/basket.js"></script>
[% ELSE %]
 <script type="text/javascript">var readCookie;</script>
[% END %]

[% IF Koha.Preference( 'opacuserlogin' ) == 1 %][% IF Koha.Preference( 'TagsEnabled' ) == 1 %]<script type="text/javascript" src="[% interface %]/[% theme %]/js/tags.js"></script>[% END %][% ELSE %][% END %]
[% IF ( GoogleJackets ) %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/google-jackets.js"></script>
 <script type="text/javascript">
        //<![CDATA[
        var NO_GOOGLE_JACKET = _("Obálka není dostupná");
        //]]>
    </script>
[% END %]
[% IF ( Koha.Preference('Coce') && Koha.Preference('CoceProviders') ) %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/coce.js"></script>
 <script type="text/javascript">
        //<![CDATA[
        var NO_COCE_JACKET = _("Obálka není dostupná");
        //]]>
    </script>
[% END %]

[% IF OpenLibraryCovers %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/openlibrary.js"></script>
 <script type="text/javascript">
    //<![CDATA[
    var NO_OL_JACKET = _("Obálka není dostupná");
    var OL_PREVIEW = _("Náhled");
    //]]>
    </script>
[% END %]

[% IF OPACLocalCoverImages %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/localcovers.js"></script>
 <script type="text/javascript">
    //<![CDATA[
    var NO_LOCAL_JACKET = _("Obálka není dostupná");
    //]]>
    </script>
[% END %]

[% IF ( BakerTaylorEnabled ) %]
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/bakertaylorimages.js"></script>
 <script type="text/javascript">
        //<![CDATA[
        var NO_BAKERTAYLOR_IMAGE = _("Obálka není dostupná");
        $(window).load(function(){
            bt_verify_images();
        });
        //]]>
    </script>
[% END %]
[% IF ( GoogleIndicTransliteration ) %]
 <script type="text/javascript" src="http://www.google.com/jsapi"></script>
 <script type="text/javascript" src="[% interface %]/[% theme %]/js/googleindictransliteration.js"></script>
[% END %]
[% IF persona %]
 <script src="https://login.persona.org/include.js"></script>
 <script type="text/javascript">

    navigator.id.watch({
        loggedInUser: [% IF emailaddress && loggedinpersona %]'[% emailaddress %]'[% ELSE %]null[% END %],
        onlogin: function (assertion) {
            $.post('/cgi-bin/koha/svc/login',
                { assertion: assertion },
                function (data) {
                    window.location = '/cgi-bin/koha/opac-user.pl';
                }
            )
            .fail(function() { var errstr = _("Přihlášení se nezdařilo, Váš e-mail v systému Persona pravděpodobně nesouhlasí s tím, který máme v evidenci");
                alert(errstr);
            });
        },
        onlogout: function () {
            window.location = '/cgi-bin/koha/opac-main.pl?logout.x=1';
        }
    });

    var signinLink = document.getElementById('browserid');

    if (signinLink) {
        signinLink.onclick = function(evt) {
            // Requests a signed identity assertion from the user.
            navigator.id.request({
                siteName: "[% LibraryName | html %]",
                returnTo: '/cgi-bin/koha/opac-user.pl',
                oncancel: function() { alert('user refuses to share identity.'); }
                });
            };
    }

    </script>
[% END #  / IF persona %]

<script type="text/javascript" src="[% interface %]/[% theme %]/lib/jquery/plugins/jquery.cookie.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    if($('#searchsubmit').length) {
        $(document).on("click", '#searchsubmit', function(e) {
            jQuery.removeCookie("form_serialized", { path: '/'});
            jQuery.removeCookie("form_serialized_itype", { path: '/'});
            jQuery.removeCookie("form_serialized_limits", { path: '/'});
            jQuery.removeCookie("num_paragraph", { path: '/'});
            jQuery.removeCookie("search_path_code", { path: '/'});
        });
    }
});
</script>
[% PROCESS jsinclude %]

[% IF ( opacuserjs ) %]
 <script type="text/javascript">
        //<![CDATA[
        [% opacuserjs %]
        //]]>
    </script>
[% END %]
</body>
</html>
