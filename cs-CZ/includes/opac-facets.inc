[% IF ( opacfacets && facets_loop && total ) %]
 <div id="search-facets">
 <h4><a href="#" class="menu-collapse-toggle">Upřesnit hledání</a></h4>
 <ul class="menu-collapse">
 <li id="availability_facet">Dostupnost: <ul>
 <li>
 [% IF ( available ) %] Zobrazuje se pouze <strong>dostupné</strong> jednotek. <a href="/cgi-bin/koha/opac-search.pl?[% query_cgi %][% limit_cgi_not_availablity %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |url %][% END %]">Zobrazit všechny položky</a>
 [% ELSE %]
 <a href="/cgi-bin/koha/opac-search.pl?[% query_cgi %][% limit_cgi |url %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |url %][% END %]&amp;limit=available">Zobrazit pouze právě dostupné dokumenty.</a>
 [% END %]
 </li>
 </ul>
 [% IF ( related ) %] <li>(související vyhledávání: [% FOREACH relate IN related %][% relate.related_search %][% END %])</li>[% END %]
 </li>

 [% FOREACH facets_loo IN facets_loop %]
 [% IF facets_loo.facets.size > 0 %]
 <li id="[% facets_loo.type_id %]">
 [% IF facets_loo.type_label_Authors %]<h5 id="facet-authors">Autoři</h5>[% END %]
 [% IF facets_loo.type_label_Titles %]<h5 id="facet-titles">Názvy</h5>[% END %]
 [% IF facets_loo.type_label_Topics %]<h5 id="facet-topics">Témata</h5>[% END %]
 [% IF facets_loo.type_label_Places %]<h5 id="facet-places">Oblast</h5>[% END %]
 [% IF facets_loo.type_label_Series %]<h5 id="facet-series">Edice</h5>[% END %]
 [% IF facets_loo.type_label_ItemTypes %]<h5 id="facet-itemtypes">Typy jednotek</h5>[% END %]
 [% UNLESS singleBranchMode %]
 [% IF ( facets_loo.type_label_HomeLibrary ) %]<span id="facet-home-libraries">Domovské knihovny</span>[% END %]
 [% IF ( facets_loo.type_label_HoldingLibrary ) %]<span id="facet-holding-libraries">Knihovna</span>[% END %]
 [% END %]
 [% IF facets_loo.type_label_Location %]<h5 id="facet-locations">Umístění</h5>[% END %]
 <ul>
 [% FOREACH facet IN facets_loo.facets %]
 <li>
 [% SET url = "/cgi-bin/koha/opac-search.pl?" _ query_cgi _ limit_cgi %]
 [% IF ( sort_by ) %]
 [% url = BLOCK %][% url %][% "&amp;sort_by=" _ sort_by |url %][% END %]
 [% END %]
 [% IF facet.active %]
 [% SET url = url _ "&amp;nolimit=" _ facet.type_link_value _ ":" _ facet.facet_link_value %]
 <span class="facet-label">[% facet.facet_label_value %]</span>
 [<a title="Smazat aspekt [% facet.facet_title_value |html %]" href="[% url %]">x</a>]
 [% ELSE %]
 [% SET url = url _ "&amp;limit=" _ facet.type_link_value _ ":" _ facet.facet_link_value %]
 <span class="facet-label"><a href="[% url %]" title="[% facet.facet_title_value |html %]">[% facet.facet_label_value %]</a></span>
 [% IF ( displayFacetCount ) %]
 <span class="facet-count"> ([% facet.facet_count %])</span>
 [% END %]
 [% END %]
 </li>
 [% END %]
 [% IF ( facets_loo.expandable ) %]
 <li class="showmore">
 <a href="/cgi-bin/koha/opac-search.pl?[% query_cgi %][% limit_cgi |url %][% IF ( sort_by ) %]&amp;sort_by=[% sort_by |url %][% END %][% IF ( offset ) %]
                                            &amp;offset=[% offset |url %][% END %]&amp;expand=[% facets_loo.expand |url %]#[% facets_loo.type_id |url %]">Zobrazit více</a>
 </li>
 [% END %]
 </ul>
 </li>
 [% END # / IF facets_loo.facets.size > 0 %]
 [% END # / FOREACH facets_loo  %]
 </ul>
 </div> <!-- / #search-facets -->

 [% IF ( OPACResultsSidebar ) %]
 <div id="opacresultssidebar">
 [% OPACResultsSidebar %]
 </div>
 [% END %]
[% END # / IF opacfacets && facets_loop %]
