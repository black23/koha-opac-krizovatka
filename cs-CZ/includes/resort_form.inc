<option value="relevance">Relevance</option>
<optgroup label="Oblíbenost">
 [% IF ( sort_by == "popularity_dsc" ) %]
 <option value="popularity_dsc" selected="selected">Oblíbenost (od nejvyšší po nejnižší)</option>
 [% ELSE %]
 <option value="popularity_dsc">Oblíbenost (od nejvyšší po nejnižší)</option>
 [% END %]
 [% IF ( sort_by == "popularity_asc" ) %]
 <option value="popularity_asc" selected="selected">Oblíbenost (od nejnižší po nejvyšší)</option>
 [% ELSE %]
 <option value="popularity_asc">Oblíbenost (od nejnižší po nejvyšší)</option>
 [% END %]
</optgroup>
<optgroup label="Autor">
 [% IF ( sort_by == "author_az" || sort_by == "author_asc" ) %]
 <option value="author_az" selected="selected">Autor (A-Z)</option>
 [% ELSE %]
 <option value="author_az">Autor (A-Z)</option>
 [% END %]
 [% IF ( sort_by == "author_za" || sort_by == "author_dsc" ) %]
 <option value="author_za" selected="selected">Autor (Z-A)</option>
 [% ELSE %]
 <option value="author_za">Autor (Z-A)</option>
 [% END %]
</optgroup>
<optgroup label="Signatura">
 [% IF ( sort_by == "call_number_asc" ) %]
 <option value="call_number_asc" selected="selected">Signatura (0-9 po A-Z)</option>
 [% ELSE %]
 <option value="call_number_asc">Signatura (0-9 po A-Z)</option>
 [% END %]
 [% IF ( sort_by == "call_number_dsc" ) %]
 <option value="call_number_dsc" selected="selected">Signatura (Z-A po 9-0)</option>
 [% ELSE %]
 <option value="call_number_dsc">Signatura (Z-A po 9-0)</option>
 [% END %]
</optgroup>
<optgroup label="Datum">
 [% IF ( sort_by == "pubdate_dsc" ) %]
 <option value="pubdate_dsc" selected="selected">Datum vydání: od nejnovějšího po nejstarší</option>
 [% ELSE %]
 <option value="pubdate_dsc">Datum vydání: od nejnovějšího po nejstarší</option>
 [% END %]
 [% IF ( sort_by == "pubdate_asc" ) %]
 <option value="pubdate_asc" selected="selected">Datum vydání: od nejstaršího po nejnovější</option>
 [% ELSE %]
 <option value="pubdate_asc">Datum vydání: od nejstaršího po nejnovější</option>
 [% END %]
 [% IF ( sort_by == "acqdate_dsc" ) %]
 <option value="acqdate_dsc" selected="selected">Datum přírustků: Od nejnovějších po nejstarší</option>
 [% ELSE %]
 <option value="acqdate_dsc">Datum přírustků: Od nejnovějších po nejstarší</option>
 [% END %]
 [% IF ( sort_by == "acqdate_asc" ) %]
 <option value="acqdate_asc" selected="selected">Datum přírustků: Od nejstarších po nejnovější</option>
 [% ELSE %]
 <option value="acqdate_asc">Datum přírustků: Od nejstarších po nejnovější</option>
 [% END %]
</optgroup>
<optgroup label="Název">
 [% IF ( sort_by == "title_az" || sort_by == "title_asc" ) %]
 <option value="title_az" selected="selected">Název (A-Ž)</option>
 [% ELSE %]
 <option value="title_az">Název (A-Ž)</option>
 [% END %]
 [% IF ( sort_by == "title_za" || sort_by == "title_dsc" ) %]
 <option value="title_za" selected="selected">Název (Ž-A)</option>
 [% ELSE %]
 <option value="title_za">Název (Ž-A)</option>
 [% END %]
</optgroup>
