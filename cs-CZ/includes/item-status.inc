[% USE AuthorisedValues %] [% SET itemavailable = 1 %] [%#- Tento vkládaný soubor může mít dva parametry: strukturu jednotky volitelně také strukturu výpůjčky. Struktura výpůjčky se používá pro stránky s rezervacemi kurzů, které nepoužívají API ke zpracování termínů vrácení jednotlivých výpůjček (item.datedue). -%] [% IF ( item.itemlost ) %] [% SET itemavailable = 0 %] [% av_lib_include = AuthorisedValues.GetByCode( 'LOST', item.itemlost, 1 ) %] [% IF ( av_lib_include ) %] <span class="item-status lost">[% av_lib_include %]</span>
 [% ELSE %]
 [% IF ( item.lostimageurl ) %]
 <img src="[% item.lostimageurl %]" alt="[% item.lostimagelabel %]" title="[% item.lostimagelabel %]" />
 [% ELSE %]
 <span class="item-status lost">Ztraceno</span>
 [% END %]
 [% END %]
[% END %]

[% IF ( item.datedue || issue.date_due ) %]
 [% SET itemavailable = 0 %]
 [% IF item.onsite_checkout %]
 [% IF ( OPACShowCheckoutName ) %]
 <span class="item-status checkedout">Půjčeno ve studovně čtenáři [% item.firstname %] [% item.surname %] [% IF ( item.cardnumber ) %] ([% item.cardnumber %]) [% END %]</span>
 [% ELSE %]
 <span class="item-status checkedout">Vypůjčeno do studovny</span>
 [% END %]
 [% ELSE %]
 [% IF ( OPACShowCheckoutName ) %]
 <span class="item-status checkedout">Vypůjčeno čtenáři [% item.firstname %] [% item.surname %]  [% IF ( item.cardnumber ) %]([% item.cardnumber %])[% END %]</span>
 [% ELSE %]
 <span class="item-status checkedout">Vypůjčeno</span>
 [% END %]
 [% END %]
[% END %]

[% IF ( item.transfertwhen ) %]
 [% SET itemavailable = 0 %]
 <span class="item-status intransit">Na cestě z [% item.transfertfrom %] do [% item.transfertto %] od [% item.transfertwhen | $KohaDates %]</span>
[% END %]

[% IF ( item.waiting ) %]
 [% SET itemavailable = 0 %]
 <span class="item-status onhold">Rezervováno</span>
[% END %]

[% IF ( item.withdrawn ) %]
 [% SET itemavailable = 0 %]
 <span class="item-status withdrawn">Odepsáno</span>
[% END %]

[% IF ( item.itemnotforloan ) %]
 [% SET itemavailable = 0 %]
 [% IF ( item.notforloanvalueopac ) %]
 <span class="item-status notforloan">[% item.notforloanvalueopac %] [% IF ( item.restrictedopac ) %]<span class="restricted">([% item.restrictedopac %])</span>[% END %]</span>
 [% ELSE %]
 <span class="item-status notforloan">Nelze vypůjčit [% IF ( item.restrictedopac ) %]<span class="restricted">([% item.restrictedopac %])</span>[% END %]</span>
 [% END %]
[% ELSIF ( item.notforloan_per_itemtype ) %]
 [% SET itemavailable = 0 %]
 <span class="item-status notforloan">Nelze vypůjčit [% IF ( item.restrictedopac ) %]<span class="restricted">([% item.restrictedopac %])</span>[% END %]</span>
[% END %]

[% IF ( item.damaged ) %]
 [% SET itemavailable = 0 %]
 [% av_lib_include = AuthorisedValues.GetByCode( 'DAMAGED', item.damaged, 1 ) %]
 [% IF av_lib_include %]
 <span class="item-status damaged">[% av_lib_include %]</span>
 [% ELSE %]
 <span class="item-status damaged">Poškozeno</span>
 [% END %]
[% END %]

[% IF item.on_order %]
 [% SET itemavailable = 0 %]
 <span class="item-status onorder">Objednáno</span>
[% END %]

[% IF ( itemavailable ) %]
 <span class="item-status available">Dostupné [% IF ( item.restrictedopac ) %]<span class="restricted">([% item.restrictedopac %])</span>[% END %]</span>
[% END %]
