<div id="search-facets">
 <form method="get" action="/cgi-bin/koha/opac-topissues.pl">
 <h4><a href="#" class="menu-collapse-toggle">Upřesnit hledání</a></h4>
 <ul class="menu-collapse">
 <li><label for="limit">Zobrazit nejoblíbenějších </label>
 <select name="limit" id="limit">
 [% IF ( limit == 10 ) %]<option value ="10" selected="selected">10 titulů</option>[% ELSE %]<option value="10">10 titulů</option>[% END %]
 [% IF ( limit == 15 ) %]<option value ="15" selected="selected">15 titulů</option>[% ELSE %]<option value="15">15 titulů</option>[% END %]
 [% IF ( limit == 20 ) %]<option value ="20" selected="selected">20 titulů</option>[% ELSE %]<option value="20">20 titulů</option>[% END %]
 [% IF ( limit == 30 ) %]<option value ="30" selected="selected">30 titulů</option>[% ELSE %]<option value="30">30 titulů</option>[% END %]
 [% IF ( limit == 40 ) %]<option value ="40" selected="selected">40 titulů</option>[% ELSE %]<option value="40">40 titulů</option>[% END %]
 [% IF ( limit == 50 ) %]<option value ="50" selected="selected">50 titulů</option>[% ELSE %]<option value="50">50 titulů</option>[% END %]
 [% IF ( limit == 100 ) %]<option value ="100" selected="selected">100 titulů</option>[% ELSE %]<option value="100">100 titulů</option>[% END %]
 </select></li>

 <li><label for="branch">Z knihovny: </label>
 <select name="branch" id="branch">
 <option value="">Všechny knihovny</option>
 [% FOREACH branchloo IN branchloop %]
 [% IF ( branchloo.selected ) %]
 <option value="[% branchloo.value %]" selected="selected">
 [% ELSE %]
 <option value="[% branchloo.value %]">
 [% END %]
 [% branchloo.branchname %]
 </option>
 [% END %]
 </select></li>

 <li><label for="itemtype">Omezit na: </label>
 <select name="itemtype" id="itemtype">
 [% IF ( ccodesearch ) %]
 <option value="">Všechny sbírky</option>
 [% ELSE %]
 <option value="">Všechny typy jednotek</option>
 [% END %]
 [% FOREACH itemtypeloo IN itemtypeloop %]
 [% IF ( itemtypeloo.selected ) %]
 <option value="[% itemtypeloo.value %]" selected="selected">
 [% ELSE %]
 <option value="[% itemtypeloo.value %]">
 [% END %]
 [% itemtypeloo.description %]
 </option>
 [% END %]
 </select></li>

 <li><label for="timeLimit">Zakoupené za posledních:</label>
 <select name="timeLimit" id="timeLimit">
 [% IF ( timeLimit == 3 ) %]
 <option value="3" selected="selected">3 měsíce</option>
 [% ELSE %]
 <option value="3">3 měsíce</option>
 [% END %]
 [% IF ( timeLimit == 6 ) %]
 <option value="6" selected="selected">6 měsíců</option>
 [% ELSE %]
 <option value="6">6 měsíců</option>
 [% END %]
 [% IF ( timeLimit == 12 ) %]
 <option value="12" selected="selected">12 měsíců</option>
 [% ELSE %]
 <option value="12">12 měsíců</option>
 [% END %]
 [% IF ( timeLimit == 999 ) %]
 <option value="999" selected="selected">Bez omezení</option>
 [% ELSE %]
 <option value="999">Bez omezení</option>
 [% END %]
 </select></li>
 <li>
 <input type="hidden" name="do_it" value="1" />
 <input value="Potvrdit" class="btn" type="submit" />
 </li>
 </ul>
 </form>
</div>
